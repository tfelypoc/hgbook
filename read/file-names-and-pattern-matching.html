<html><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8"><title>Chapter 7. File names and pattern matching</title><link rel="stylesheet" href="/support/styles.css" type="text/css"><meta name="generator" content="DocBook XSL Stylesheets V1.74.3"><link rel="home" href="index.html" title="Mercurial: The Definitive Guide"><link rel="up" href="index.html" title="Mercurial: The Definitive Guide"><link rel="prev" href="collaborating-with-other-people.html" title="Chapter 6. Collaborating with other people"><link rel="next" href="managing-releases-and-branchy-development.html" title="Chapter 8. Managing releases and branchy development"><link rel="alternate" type="application/atom+xml" title="Comments" href="/feeds/comments/"><link rel="shortcut icon" type="image/png" href="/support/figs/favicon.png"><script type="text/javascript" src="/support/jquery-min.js"></script><script type="text/javascript" src="/support/form.js"></script><script type="text/javascript" src="/support/hsbook.js"></script></head><body bgcolor="white" text="black" link="#0000FF" vlink="#840084" alink="#0000FF"><div class="navheader"><h2 class="booktitle"><a href="/">Mercurial: The Definitive Guide</a><span class="authors">by Bryan O'Sullivan</span></h2></div><div class="navheader"><table width="100%" summary="Navigation header"><tr><th colspan="3" align="center">Chapter 7. File names and pattern matching</th></tr><tr><td width="20%" align="left"><a accesskey="p" href="collaborating-with-other-people.html">Prev</a> </td><th width="60%" align="center"> </th><td width="20%" align="right"> <a accesskey="n" href="managing-releases-and-branchy-development.html">Next</a></td></tr></table></div><div class="chapter" lang="en" id="chap:names"><div class="titlepage"><div><div><h2 class="title">Chapter 7. File names and pattern matching</h2></div></div></div><div class="toc"><p><b>Table of Contents</b></p><dl><dt><span class="sect1"><a href="file-names-and-pattern-matching.html#id380420">Simple file naming</a></span></dt><dt><span class="sect1"><a href="file-names-and-pattern-matching.html#id380836">Running commands without any file names</a></span></dt><dt><span class="sect1"><a href="file-names-and-pattern-matching.html#id381125">Telling you what's going on</a></span></dt><dt><span class="sect1"><a href="file-names-and-pattern-matching.html#id381184">Using patterns to identify files</a></span></dt><dd><dl><dt><span class="sect2"><a href="file-names-and-pattern-matching.html#id381291">Shell-style glob patterns</a></span></dt><dd><dl><dt><span class="sect3"><a href="file-names-and-pattern-matching.html#id381653">Watch out!</a></span></dt></dl></dd><dt><span class="sect2"><a href="file-names-and-pattern-matching.html#id381804">Regular expression matching with re
	patterns</a></span></dt></dl></dd><dt><span class="sect1"><a href="file-names-and-pattern-matching.html#id381878">Filtering files</a></span></dt><dt><span class="sect1"><a href="file-names-and-pattern-matching.html#id382104">Permanently ignoring unwanted files and directories</a></span></dt><dt><span class="sect1"><a href="file-names-and-pattern-matching.html#sec:names:case">Case sensitivity</a></span></dt><dd><dl><dt><span class="sect2"><a href="file-names-and-pattern-matching.html#id382327">Safe, portable repository storage</a></span></dt><dt><span class="sect2"><a href="file-names-and-pattern-matching.html#id382348">Detecting case conflicts</a></span></dt><dt><span class="sect2"><a href="file-names-and-pattern-matching.html#id382414">Fixing a case conflict</a></span></dt></dl></dd></dl></div><p id="x_543"><a name="x_543"></a>Mercurial provides mechanisms that let you work with file
    names in a consistent and expressive way.</p><div class="sect1" lang="en"><div class="titlepage"><div><div><h2 class="title" style="clear: both" id="id380420">Simple file naming</h2></div></div></div><p id="x_544"><a name="x_544"></a>Mercurial uses a unified piece of machinery “<span class="quote">under the
	hood</span>” to handle file names.  Every command behaves
      uniformly with respect to file names.  The way in which commands
      work with file names is as follows.</p><p id="x_545"><a name="x_545"></a>If you explicitly name real files on the command line,
      Mercurial works with exactly those files, as you would expect.
      
</p><pre id="id381103" class="screen"><code class="prompt">$</code> <strong class="userinput"><code>hg add COPYING README examples/simple.py</code></strong>
</pre><p>

</p><p id="x_546"><a name="x_546"></a>When you provide a directory name, Mercurial will interpret
      this as “<span class="quote">operate on every file in this directory and its
	subdirectories</span>”. Mercurial traverses the files and
      subdirectories in a directory in alphabetical order.  When it
      encounters a subdirectory, it will traverse that subdirectory
      before continuing with the current directory.</p><pre id="id381083" class="screen"><code class="prompt">$</code> <strong class="userinput"><code>hg status src</code></strong>
? src/main.py
? src/watcher/_watcher.c
? src/watcher/watcher.py
? src/xyzzy.txt
</pre></div><div class="sect1" lang="en"><div class="titlepage"><div><div><h2 class="title" style="clear: both" id="id380836">Running commands without any file names</h2></div></div></div><p id="x_547"><a name="x_547"></a>Mercurial's commands that work with file names have useful
      default behaviors when you invoke them without providing any
      file names or patterns.  What kind of behavior you should
      expect depends on what the command does.  Here are a few rules
      of thumb you can use to predict what a command is likely to do
      if you don't give it any names to work with.</p><div class="itemizedlist"><ul type="disc"><li><p id="x_548"><a name="x_548"></a>Most commands will operate on the entire working
	  directory. This is what the <span class="command"><strong>hg
	    add</strong></span> command does, for example.</p></li><li><p id="x_549"><a name="x_549"></a>If the command has effects that are difficult or
	  impossible to reverse, it will force you to explicitly
	  provide at least one name or pattern (see below).  This
	  protects you from accidentally deleting files by running
	  <span class="command"><strong>hg remove</strong></span> with no
	  arguments, for example.</p></li></ul></div><p id="x_54a"><a name="x_54a"></a>It's easy to work around these default behaviors if they
      don't suit you.  If a command normally operates on the whole
      working directory, you can invoke it on just the current
      directory and its subdirectories by giving it the name
      “<span class="quote"><code class="filename">.</code></span>”.</p><pre id="id381050" class="screen"><code class="prompt">$</code> <strong class="userinput"><code>cd src</code></strong>
<code class="prompt">$</code> <strong class="userinput"><code>hg add -n</code></strong>
adding ../MANIFEST.in
adding ../examples/performant.py
adding ../setup.py
adding main.py
adding watcher/_watcher.c
adding watcher/watcher.py
adding xyzzy.txt
<code class="prompt">$</code> <strong class="userinput"><code>hg add -n .</code></strong>
adding main.py
adding watcher/_watcher.c
adding watcher/watcher.py
adding xyzzy.txt
</pre><p id="x_54b"><a name="x_54b"></a>Along the same lines, some commands normally print file
      names relative to the root of the repository, even if you're
      invoking them from a subdirectory.  Such a command will print
      file names relative to your subdirectory if you give it explicit
      names.  Here, we're going to run <span class="command"><strong>hg
	status</strong></span> from a subdirectory, and get it to operate on
      the entire working directory while printing file names relative
      to our subdirectory, by passing it the output of the <span class="command"><strong>hg root</strong></span> command.</p><pre id="id381404" class="screen"><code class="prompt">$</code> <strong class="userinput"><code>hg status</code></strong>
A COPYING
A README
A examples/simple.py
? MANIFEST.in
? examples/performant.py
? setup.py
? src/main.py
? src/watcher/_watcher.c
? src/watcher/watcher.py
? src/xyzzy.txt
<code class="prompt">$</code> <strong class="userinput"><code>hg status `hg root`</code></strong>
A ../COPYING
A ../README
A ../examples/simple.py
? ../MANIFEST.in
? ../examples/performant.py
? ../setup.py
? main.py
? watcher/_watcher.c
? watcher/watcher.py
? xyzzy.txt
</pre></div><div class="sect1" lang="en"><div class="titlepage"><div><div><h2 class="title" style="clear: both" id="id381125">Telling you what's going on</h2></div></div></div><p id="x_54c"><a name="x_54c"></a>The <span class="command"><strong>hg add</strong></span> example in the
      preceding section illustrates something else that's helpful
      about Mercurial commands.  If a command operates on a file that
      you didn't name explicitly on the command line, it will usually
      print the name of the file, so that you will not be surprised
      what's going on.</p><p id="x_54d"><a name="x_54d"></a>The principle here is of <span class="emphasis"><em>least
	surprise</em></span>.  If you've exactly named a file on the
      command line, there's no point in repeating it back at you.  If
      Mercurial is acting on a file <span class="emphasis"><em>implicitly</em></span>, e.g.
      because you provided no names, or a directory, or a pattern (see
      below), it is safest to tell you what files it's operating on.</p><p id="x_54e"><a name="x_54e"></a>For commands that behave this way, you can silence them
      using the <code class="option">-q</code> option.  You
      can also get them to print the name of every file, even those
      you've named explicitly, using the <code class="option">-v</code> option.</p></div><div class="sect1" lang="en"><div class="titlepage"><div><div><h2 class="title" style="clear: both" id="id381184">Using patterns to identify files</h2></div></div></div><p id="x_54f"><a name="x_54f"></a>In addition to working with file and directory names,
      Mercurial lets you use <span class="emphasis"><em>patterns</em></span> to identify
      files.  Mercurial's pattern handling is expressive.</p><p id="x_550"><a name="x_550"></a>On Unix-like systems (Linux, MacOS, etc.), the job of
      matching file names to patterns normally falls to the shell.  On
      these systems, you must explicitly tell Mercurial that a name is
      a pattern.  On Windows, the shell does not expand patterns, so
      Mercurial will automatically identify names that are patterns,
      and expand them for you.</p><p id="x_551"><a name="x_551"></a>To provide a pattern in place of a regular name on the
      command line, the mechanism is simple:</p><pre id="id381219" class="programlisting">syntax:patternbody</pre><p id="x_552"><a name="x_552"></a>That is, a pattern is identified by a short text string that
      says what kind of pattern this is, followed by a colon, followed
      by the actual pattern.</p><p id="x_553"><a name="x_553"></a>Mercurial supports two kinds of pattern syntax.  The most
      frequently used is called <code class="literal">glob</code>; this is the
      same kind of pattern matching used by the Unix shell, and should
      be familiar to Windows command prompt users, too.</p><p id="x_554"><a name="x_554"></a>When Mercurial does automatic pattern matching on Windows,
      it uses <code class="literal">glob</code> syntax.  You can thus omit the
      “<span class="quote"><code class="literal">glob:</code></span>” prefix on Windows, but
      it's safe to use it, too.</p><p id="x_555"><a name="x_555"></a>The <code class="literal">re</code> syntax is more powerful; it lets
      you specify patterns using regular expressions, also known as
      regexps.</p><p id="x_556"><a name="x_556"></a>By the way, in the examples that follow, notice that I'm
      careful to wrap all of my patterns in quote characters, so that
      they won't get expanded by the shell before Mercurial sees
      them.</p><div class="sect2" lang="en"><div class="titlepage"><div><div><h3 class="title" id="id381291">Shell-style <code class="literal">glob</code> patterns</h3></div></div></div><p id="x_557"><a name="x_557"></a>This is an overview of the kinds of patterns you can use
	when you're matching on glob patterns.</p><p id="x_558"><a name="x_558"></a>The “<span class="quote"><code class="literal">*</code></span>” character matches
	any string, within a single directory.</p><pre id="id381387" class="screen"><code class="prompt">$</code> <strong class="userinput"><code>hg add 'glob:*.py'</code></strong>
adding main.py
</pre><p id="x_559"><a name="x_559"></a>The “<span class="quote"><code class="literal">**</code></span>” pattern matches
	any string, and crosses directory boundaries.  It's not a
	standard Unix glob token, but it's accepted by several popular
	Unix shells, and is very useful.</p><pre id="id381744" class="screen"><code class="prompt">$</code> <strong class="userinput"><code>cd ..</code></strong>
<code class="prompt">$</code> <strong class="userinput"><code>hg status 'glob:**.py'</code></strong>
A examples/simple.py
A src/main.py
? examples/performant.py
? setup.py
? src/watcher/watcher.py
</pre><p id="x_55a"><a name="x_55a"></a>The “<span class="quote"><code class="literal">?</code></span>” pattern matches
	any single character.</p><pre id="id381494" class="screen"><code class="prompt">$</code> <strong class="userinput"><code>hg status 'glob:**.?'</code></strong>
? src/watcher/_watcher.c
</pre><p id="x_55b"><a name="x_55b"></a>The “<span class="quote"><code class="literal">[</code></span>” character begins a
	<span class="emphasis"><em>character class</em></span>.  This matches any single
	character within the class.  The class ends with a
	“<span class="quote"><code class="literal">]</code></span>” character.  A class may
	contain multiple <span class="emphasis"><em>range</em></span>s of the form
	“<span class="quote"><code class="literal">a-f</code></span>”, which is shorthand for
	“<span class="quote"><code class="literal">abcdef</code></span>”.</p><pre id="id381693" class="screen"><code class="prompt">$</code> <strong class="userinput"><code>hg status 'glob:**[nr-t]'</code></strong>
? MANIFEST.in
? src/xyzzy.txt
</pre><p id="x_55c"><a name="x_55c"></a>If the first character after the
	“<span class="quote"><code class="literal">[</code></span>” in a character class is a
	“<span class="quote"><code class="literal">!</code></span>”, it
	<span class="emphasis"><em>negates</em></span> the class, making it match any
	single character not in the class.</p><p id="x_55d"><a name="x_55d"></a>A “<span class="quote"><code class="literal">{</code></span>” begins a group of
	subpatterns, where the whole group matches if any subpattern
	in the group matches.  The “<span class="quote"><code class="literal">,</code></span>”
	character separates subpatterns, and
	“<span class="quote"><code class="literal">}</code></span>” ends the group.</p><pre id="id381665" class="screen"><code class="prompt">$</code> <strong class="userinput"><code>hg status 'glob:*.{in,py}'</code></strong>
? MANIFEST.in
? setup.py
</pre><div class="sect3" lang="en"><div class="titlepage"><div><div><h4 class="title" id="id381653">Watch out!</h4></div></div></div><p id="x_55e"><a name="x_55e"></a>Don't forget that if you want to match a pattern in any
	  directory, you should not be using the
	  “<span class="quote"><code class="literal">*</code></span>” match-any token, as this
	  will only match within one directory.  Instead, use the
	  “<span class="quote"><code class="literal">**</code></span>” token.  This small
	  example illustrates the difference between the two.</p><pre id="id382086" class="screen"><code class="prompt">$</code> <strong class="userinput"><code>hg status 'glob:*.py'</code></strong>
? setup.py
<code class="prompt">$</code> <strong class="userinput"><code>hg status 'glob:**.py'</code></strong>
A examples/simple.py
A src/main.py
? examples/performant.py
? setup.py
? src/watcher/watcher.py
</pre></div></div><div class="sect2" lang="en"><div class="titlepage"><div><div><h3 class="title" id="id381804">Regular expression matching with <code class="literal">re</code>
	patterns</h3></div></div></div><p id="x_55f"><a name="x_55f"></a>Mercurial accepts the same regular expression syntax as
	the Python programming language (it uses Python's regexp
	engine internally). This is based on the Perl language's
	regexp syntax, which is the most popular dialect in use (it's
	also used in Java, for example).</p><p id="x_560"><a name="x_560"></a>I won't discuss Mercurial's regexp dialect in any detail
	here, as regexps are not often used.  Perl-style regexps are
	in any case already exhaustively documented on a multitude of
	web sites, and in many books.  Instead, I will focus here on a
	few things you should know if you find yourself needing to use
	regexps with Mercurial.</p><p id="x_561"><a name="x_561"></a>A regexp is matched against an entire file name, relative
	to the root of the repository.  In other words, even if you're
	already in subbdirectory <code class="filename">foo</code>, if you want to match files
	under this directory, your pattern must start with
	“<span class="quote"><code class="literal">foo/</code></span>”.</p><p id="x_562"><a name="x_562"></a>One thing to note, if you're familiar with Perl-style
	regexps, is that Mercurial's are <span class="emphasis"><em>rooted</em></span>.
	That is, a regexp starts matching against the beginning of a
	string; it doesn't look for a match anywhere within the
	string.  To match anywhere in a string, start your pattern
	with “<span class="quote"><code class="literal">.*</code></span>”.</p></div></div><div class="sect1" lang="en"><div class="titlepage"><div><div><h2 class="title" style="clear: both" id="id381878">Filtering files</h2></div></div></div><p id="x_563"><a name="x_563"></a>Not only does Mercurial give you a variety of ways to
      specify files; it lets you further winnow those files using
      <span class="emphasis"><em>filters</em></span>.  Commands that work with file
      names accept two filtering options.</p><div class="itemizedlist"><ul type="disc"><li><p id="x_564"><a name="x_564"></a><code class="option">-I</code>, or
	  <code class="option">--include</code>, lets you
	  specify a pattern that file names must match in order to be
	  processed.</p></li><li><p id="x_565"><a name="x_565"></a><code class="option">-X</code>, or
	  <code class="option">--exclude</code>, gives you a
	  way to <span class="emphasis"><em>avoid</em></span> processing files, if they
	  match this pattern.</p></li></ul></div><p id="x_566"><a name="x_566"></a>You can provide multiple <code class="option">-I</code> and <code class="option">-X</code> options on the command line,
      and intermix them as you please.  Mercurial interprets the
      patterns you provide using glob syntax by default (but you can
      use regexps if you need to).</p><p id="x_567"><a name="x_567"></a>You can read a <code class="option">-I</code>
      filter as “<span class="quote">process only the files that match this
	filter</span>”.</p><pre id="id382042" class="screen"><code class="prompt">$</code> <strong class="userinput"><code>hg status -I '*.in'</code></strong>
? MANIFEST.in
</pre><p id="x_568"><a name="x_568"></a>The <code class="option">-X</code> filter is best
      read as “<span class="quote">process only the files that don't match this
	pattern</span>”.</p><pre id="id382014" class="screen"><code class="prompt">$</code> <strong class="userinput"><code>hg status -X '**.py' src</code></strong>
? src/watcher/_watcher.c
? src/xyzzy.txt
</pre></div><div class="sect1" lang="en"><div class="titlepage"><div><div><h2 class="title" style="clear: both" id="id382104">Permanently ignoring unwanted files and directories</h2></div></div></div><p id="x_569"><a name="x_569"></a>When you create a new repository, the chances are
      that over time it will grow to contain files that ought to
      <span class="emphasis"><em>not</em></span> be managed by Mercurial, but which you
      don't want to see listed every time you run <span class="command"><strong>hg
	status</strong></span>.  For instance, “<span class="quote">build products</span>”
      are files that are created as part of a build but which should
      not be managed by a revision control system.  The most common
      build products are output files produced by software tools such
      as compilers.  As another example, many text editors litter a
      directory with lock files, temporary working files, and backup
      files, which it also makes no sense to manage.</p><p id="x_6b4"><a name="x_6b4"></a>To have Mercurial permanently ignore such files, create a
      file named <code class="filename">.hgignore</code> in the root of your
      repository.  You <span class="emphasis"><em>should</em></span> <span class="command"><strong>hg
      add</strong></span> this file so that it gets tracked with the rest of
      your repository contents, since your collaborators will probably
      find it useful too.</p><p id="x_6b5"><a name="x_6b5"></a>By default, the <code class="filename">.hgignore</code> file should
      contain a list of regular expressions, one per line.  Empty
      lines are skipped. Most people prefer to describe the files they
      want to ignore using the “<span class="quote">glob</span>” syntax that we
      described above, so a typical <code class="filename">.hgignore</code>
      file will start with this directive:</p><pre id="id382178" class="programlisting">syntax: glob</pre><p id="x_6b6"><a name="x_6b6"></a>This tells Mercurial to interpret the lines that follow as
      glob patterns, not regular expressions.</p><p id="x_6b7"><a name="x_6b7"></a>Here is a typical-looking <code class="filename">.hgignore</code>
      file.</p><pre id="id382204" class="programlisting">syntax: glob
# This line is a comment, and will be skipped.
# Empty lines are skipped too.

# Backup files left behind by the Emacs editor.
*~

# Lock files used by the Emacs editor.
# Notice that the "#" character is quoted with a backslash.
# This prevents it from being interpreted as starting a comment.
.\#*

# Temporary files used by the vim editor.
.*.swp

# A hidden file created by the Mac OS X Finder.
.DS_Store
</pre></div><div class="sect1" lang="en"><div class="titlepage"><div><div><h2 class="title" style="clear: both" id="sec:names:case">Case sensitivity</h2></div></div></div><p id="x_56a"><a name="x_56a"></a>If you're working in a mixed development environment that
      contains both Linux (or other Unix) systems and Macs or Windows
      systems, you should keep in the back of your mind the knowledge
      that they treat the case (“<span class="quote">N</span>” versus
      “<span class="quote">n</span>”) of file names in incompatible ways.  This is
      not very likely to affect you, and it's easy to deal with if it
      does, but it could surprise you if you don't know about
      it.</p><p id="x_56b"><a name="x_56b"></a>Operating systems and filesystems differ in the way they
      handle the <span class="emphasis"><em>case</em></span> of characters in file and
      directory names.  There are three common ways to handle case in
      names.</p><div class="itemizedlist"><ul type="disc"><li><p id="x_56c"><a name="x_56c"></a>Completely case insensitive.  Uppercase and
	  lowercase versions of a letter are treated as identical,
	  both when creating a file and during subsequent accesses.
	  This is common on older DOS-based systems.</p></li><li><p id="x_56d"><a name="x_56d"></a>Case preserving, but insensitive.  When a file
	  or directory is created, the case of its name is stored, and
	  can be retrieved and displayed by the operating system.
	  When an existing file is being looked up, its case is
	  ignored.  This is the standard arrangement on Windows and
	  MacOS.  The names <code class="filename">foo</code> and
	  <code class="filename">FoO</code> identify the same file.  This
	  treatment of uppercase and lowercase letters as
	  interchangeable is also referred to as <span class="emphasis"><em>case
	    folding</em></span>.</p></li><li><p id="x_56e"><a name="x_56e"></a>Case sensitive.  The case of a name
	  is significant at all times. The names
	  <code class="filename">foo</code> and <code class="filename">FoO</code>
	  identify different files.  This is the way Linux and Unix
	  systems normally work.</p></li></ul></div><p id="x_56f"><a name="x_56f"></a>On Unix-like systems, it is possible to have any or all of
      the above ways of handling case in action at once.  For example,
      if you use a USB thumb drive formatted with a FAT32 filesystem
      on a Linux system, Linux will handle names on that filesystem in
      a case preserving, but insensitive, way.</p><div class="sect2" lang="en"><div class="titlepage"><div><div><h3 class="title" id="id382327">Safe, portable repository storage</h3></div></div></div><p id="x_570"><a name="x_570"></a>Mercurial's repository storage mechanism is <span class="emphasis"><em>case
	  safe</em></span>.  It translates file names so that they can
	be safely stored on both case sensitive and case insensitive
	filesystems.  This means that you can use normal file copying
	tools to transfer a Mercurial repository onto, for example, a
	USB thumb drive, and safely move that drive and repository
	back and forth between a Mac, a PC running Windows, and a
	Linux box.</p></div><div class="sect2" lang="en"><div class="titlepage"><div><div><h3 class="title" id="id382348">Detecting case conflicts</h3></div></div></div><p id="x_571"><a name="x_571"></a>When operating in the working directory, Mercurial honours
	the naming policy of the filesystem where the working
	directory is located.  If the filesystem is case preserving,
	but insensitive, Mercurial will treat names that differ only
	in case as the same.</p><p id="x_572"><a name="x_572"></a>An important aspect of this approach is that it is
	possible to commit a changeset on a case sensitive (typically
	Linux or Unix) filesystem that will cause trouble for users on
	case insensitive (usually Windows and MacOS) users.  If a
	Linux user commits changes to two files, one named
	<code class="filename">myfile.c</code> and the other named
	<code class="filename">MyFile.C</code>, they will be stored correctly
	in the repository.  And in the working directories of other
	Linux users, they will be correctly represented as separate
	files.</p><p id="x_573"><a name="x_573"></a>If a Windows or Mac user pulls this change, they will not
	initially have a problem, because Mercurial's repository
	storage mechanism is case safe.  However, once they try to
	<span class="command"><strong>hg update</strong></span> the working
	directory to that changeset, or <span class="command"><strong>hg
	  merge</strong></span> with that changeset, Mercurial will spot the
	conflict between the two file names that the filesystem would
	treat as the same, and forbid the update or merge from
	occurring.</p></div><div class="sect2" lang="en"><div class="titlepage"><div><div><h3 class="title" id="id382414">Fixing a case conflict</h3></div></div></div><p id="x_574"><a name="x_574"></a>If you are using Windows or a Mac in a mixed environment
	where some of your collaborators are using Linux or Unix, and
	Mercurial reports a case folding conflict when you try to
	<span class="command"><strong>hg update</strong></span> or <span class="command"><strong>hg merge</strong></span>, the procedure to fix the
	problem is simple.</p><p id="x_575"><a name="x_575"></a>Just find a nearby Linux or Unix box, clone the problem
	repository onto it, and use Mercurial's <span class="command"><strong>hg rename</strong></span> command to change the
	names of any offending files or directories so that they will
	no longer cause case folding conflicts.  Commit this change,
	<span class="command"><strong>hg pull</strong></span> or <span class="command"><strong>hg push</strong></span> it across to your Windows or
	MacOS system, and <span class="command"><strong>hg update</strong></span>
	to the revision with the non-conflicting names.</p><p id="x_576"><a name="x_576"></a>The changeset with case-conflicting names will remain in
	your project's history, and you still won't be able to
	<span class="command"><strong>hg update</strong></span> your working
	directory to that changeset on a Windows or MacOS system, but
	you can continue development unimpeded.</p></div></div></div><div class="hgfooter"><p><img src="/support/figs/rss.png"> Want to stay up to date? Subscribe to the comment feed for <a id="chapterfeed" class="feed" href="/feeds/comments/">this chapter</a>, or the <a class="feed" href="/feeds/comments/">entire book</a>.</p><p>Copyright 2006, 2007, 2008, 2009 Bryan O'Sullivan.
      Icons by <a href="mailto:mattahan@gmail.com">Paul Davey</a> aka <a href="http://mattahan.deviantart.com/">Mattahan</a>.</p></div><div class="navfooter"><table width="100%" summary="Navigation footer"><tr><td width="40%" align="left"><a accesskey="p" href="collaborating-with-other-people.html">Prev</a> </td><td width="20%" align="center"> </td><td width="40%" align="right"> <a accesskey="n" href="managing-releases-and-branchy-development.html">Next</a></td></tr><tr><td width="40%" align="left" valign="top">Chapter 6. Collaborating with other people </td><td width="20%" align="center"><a accesskey="h" href="index.html">Home</a></td><td width="40%" align="right" valign="top"> Chapter 8. Managing releases and branchy development</td></tr></table></div><script type="text/javascript">
    var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
    document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
    </script><script type="text/javascript">
    try {
    var pageTracker = _gat._getTracker("UA-1805907-5");
    pageTracker._trackPageview();
    } catch(err) {}</script></body></html>
