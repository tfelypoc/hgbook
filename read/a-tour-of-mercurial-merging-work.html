<html><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8"><title>Chapter 3. A tour of Mercurial: merging work</title><link rel="stylesheet" href="/support/styles.css" type="text/css"><meta name="generator" content="DocBook XSL Stylesheets V1.74.3"><link rel="home" href="index.html" title="Mercurial: The Definitive Guide"><link rel="up" href="index.html" title="Mercurial: The Definitive Guide"><link rel="prev" href="a-tour-of-mercurial-the-basics.html" title="Chapter 2. A tour of Mercurial: the basics"><link rel="next" href="behind-the-scenes.html" title="Chapter 4. Behind the scenes"><link rel="alternate" type="application/atom+xml" title="Comments" href="/feeds/comments/"><link rel="shortcut icon" type="image/png" href="/support/figs/favicon.png"><script type="text/javascript" src="/support/jquery-min.js"></script><script type="text/javascript" src="/support/form.js"></script><script type="text/javascript" src="/support/hsbook.js"></script></head><body bgcolor="white" text="black" link="#0000FF" vlink="#840084" alink="#0000FF"><div class="navheader"><h2 class="booktitle"><a href="/">Mercurial: The Definitive Guide</a><span class="authors">by Bryan O'Sullivan</span></h2></div><div class="navheader"><table width="100%" summary="Navigation header"><tr><th colspan="3" align="center">Chapter 3. A tour of Mercurial: merging work</th></tr><tr><td width="20%" align="left"><a accesskey="p" href="a-tour-of-mercurial-the-basics.html">Prev</a> </td><th width="60%" align="center"> </th><td width="20%" align="right"> <a accesskey="n" href="behind-the-scenes.html">Next</a></td></tr></table></div><div class="chapter" lang="en" id="chap:tour-merge"><div class="titlepage"><div><div><h2 class="title">Chapter 3. A tour of Mercurial: merging work</h2></div></div></div><div class="toc"><p><b>Table of Contents</b></p><dl><dt><span class="sect1"><a href="a-tour-of-mercurial-merging-work.html#id352947">Merging streams of work</a></span></dt><dd><dl><dt><span class="sect2"><a href="a-tour-of-mercurial-merging-work.html#id353683">Head changesets</a></span></dt><dt><span class="sect2"><a href="a-tour-of-mercurial-merging-work.html#id353830">Performing the merge</a></span></dt><dt><span class="sect2"><a href="a-tour-of-mercurial-merging-work.html#id353997">Committing the results of the merge</a></span></dt></dl></dd><dt><span class="sect1"><a href="a-tour-of-mercurial-merging-work.html#id354262">Merging conflicting changes</a></span></dt><dd><dl><dt><span class="sect2"><a href="a-tour-of-mercurial-merging-work.html#id354379">Using a graphical merge tool</a></span></dt><dt><span class="sect2"><a href="a-tour-of-mercurial-merging-work.html#id354524">A worked example</a></span></dt></dl></dd><dt><span class="sect1"><a href="a-tour-of-mercurial-merging-work.html#sec:tour-merge:fetch">Simplifying the pull-merge-commit sequence</a></span></dt><dt><span class="sect1"><a href="a-tour-of-mercurial-merging-work.html#id355904">Renaming, copying, and merging</a></span></dt></dl></div><p id="x_338"><a name="x_338"></a>We've now covered cloning a repository, making changes in a
    repository, and pulling or pushing changes from one repository
    into another.  Our next step is <span class="emphasis"><em>merging</em></span>
    changes from separate repositories.</p><div class="sect1" lang="en"><div class="titlepage"><div><div><h2 class="title" style="clear: both" id="id352947">Merging streams of work</h2></div></div></div><p id="x_339"><a name="x_339"></a>Merging is a fundamental part of working with a distributed
      revision control tool.  Here are a few cases in which the need
      to merge work arises.</p><div class="itemizedlist"><ul type="disc"><li><p id="x_33a"><a name="x_33a"></a>Alice and Bob each have a personal copy of a
	  repository for a project they're collaborating on.  Alice
	  fixes a bug in her repository; Bob adds a new feature in
	  his.  They want the shared repository to contain both the
	  bug fix and the new feature.</p></li><li><p id="x_33b"><a name="x_33b"></a>Cynthia frequently works on several different
	  tasks for a single project at once, each safely isolated in
	  its own repository. Working this way means that she often
	  needs to merge one piece of her own work with
	  another.</p></li></ul></div><p id="x_33c"><a name="x_33c"></a>Because we need to merge often, Mercurial makes
      the process easy.  Let's walk through a merge.  We'll begin by
      cloning yet another repository (see how often they spring up?)
      and making a change in it.</p><pre id="id353643" class="screen"><code class="prompt">$</code> <strong class="userinput"><code>cd ..</code></strong>
<code class="prompt">$</code> <strong class="userinput"><code>hg clone hello my-new-hello</code></strong>
updating working directory
2 files updated, 0 files merged, 0 files removed, 0 files unresolved
<code class="prompt">$</code> <strong class="userinput"><code>cd my-new-hello</code></strong>
# Make some simple edits to hello.c.
<code class="prompt">$</code> <strong class="userinput"><code>my-text-editor hello.c</code></strong>
<code class="prompt">$</code> <strong class="userinput"><code>hg commit -m 'A new hello for a new day.'</code></strong>
</pre><p id="x_33d"><a name="x_33d"></a>We should now have two copies of
      <code class="filename">hello.c</code> with different contents.  The
      histories of the two repositories have also diverged, as
      illustrated in <a class="xref" href="a-tour-of-mercurial-merging-work.html#fig:tour-merge:sep-repos" title="Figure 3.1. Divergent recent histories of the my-hello and my-new-hello repositories">Figure 3.1, “Divergent recent histories of the my-hello and my-new-hello
	repositories”</a>.  Here is a copy of our
      file from one repository.</p><pre id="id353616" class="screen"><code class="prompt">$</code> <strong class="userinput"><code>cat hello.c</code></strong>
/*
 * Placed in the public domain by Bryan O'Sullivan.  This program is
 * not covered by patents in the United States or other countries.
 */

#include &lt;stdio.h&gt;

int main(int argc, char **argv)
{
	printf("once more, hello.\n");
	printf("hello, world!\");
	printf("hello again!\n");
	return 0;
}
</pre><p id="x_722"><a name="x_722"></a>And here is our slightly different version from the other
      repository.</p><pre id="id353585" class="screen"><code class="prompt">$</code> <strong class="userinput"><code>cat ../my-hello/hello.c</code></strong>
/*
 * Placed in the public domain by Bryan O'Sullivan.  This program is
 * not covered by patents in the United States or other countries.
 */

#include &lt;stdio.h&gt;

int main(int argc, char **argv)
{
	printf("hello, world!\");
	printf("hello again!\n");
	return 0;
}
</pre><div class="figure"><a name="fig:tour-merge:sep-repos"></a><p class="title"><b>Figure 3.1. Divergent recent histories of the <code class="filename">my-hello</code> and <code class="filename">my-new-hello</code>
	repositories</b></p><div class="figure-contents"><div class="mediaobject"><img src="figs/tour-merge-sep-repos.png" alt="XXX add text"></div></div></div><br class="figure-break"><p id="x_33f"><a name="x_33f"></a>We already know that pulling changes from our <code class="filename">my-hello</code> repository will have no
      effect on the working directory.</p><pre id="id353533" class="screen"><code class="prompt">$</code> <strong class="userinput"><code>hg pull ../my-hello</code></strong>
pulling from ../my-hello
searching for changes
adding changesets
adding manifests
adding file changes
added 1 changesets with 1 changes to 1 files (+1 heads)
(run 'hg heads' to see heads, 'hg merge' to merge)
</pre><p id="x_340"><a name="x_340"></a>However, the <span class="command"><strong>hg pull</strong></span>
      command says something about “<span class="quote">heads</span>”.</p><div class="sect2" lang="en"><div class="titlepage"><div><div><h3 class="title" id="id353683">Head changesets</h3></div></div></div><p id="x_341"><a name="x_341"></a>Remember that Mercurial records what the parent
	of each change is.  If a change has a parent, we call it a
	child or descendant of the parent.  A head is a change that
	has no children.  The tip revision is thus a head, because the
	newest revision in a repository doesn't have any children.
	There are times when a repository can contain more than one
	head.</p><div class="figure"><a name="fig:tour-merge:pull"></a><p class="title"><b>Figure 3.2. Repository contents after pulling from <code class="filename">my-hello</code> into <code class="filename">my-new-hello</code></b></p><div class="figure-contents"><div class="mediaobject"><img src="figs/tour-merge-pull.png" alt="XXX add text"></div></div></div><br class="figure-break"><p id="x_343"><a name="x_343"></a>In <a class="xref" href="a-tour-of-mercurial-merging-work.html#fig:tour-merge:pull" title="Figure 3.2. Repository contents after pulling from my-hello into my-new-hello">Figure 3.2, “Repository contents after pulling from my-hello into my-new-hello”</a>, you can
	see the effect of the pull from <code class="filename">my-hello</code> into <code class="filename">my-new-hello</code>.  The history that
	was already present in <code class="filename">my-new-hello</code> is untouched, but
	a new revision has been added.  By referring to <a class="xref" href="a-tour-of-mercurial-merging-work.html#fig:tour-merge:sep-repos" title="Figure 3.1. Divergent recent histories of the my-hello and my-new-hello repositories">Figure 3.1, “Divergent recent histories of the my-hello and my-new-hello
	repositories”</a>, we can see that the
	<span class="emphasis"><em>changeset ID</em></span> remains the same in the new
	repository, but the <span class="emphasis"><em>revision number</em></span> has
	changed.  (This, incidentally, is a fine example of why it's
	not safe to use revision numbers when discussing changesets.)
	We can view the heads in a repository using the <span class="command"><strong>hg heads</strong></span> command.</p><pre id="id354126" class="screen"><code class="prompt">$</code> <strong class="userinput"><code>hg heads</code></strong>
changeset:   6:b6fed4f21233
tag:         tip
parent:      4:2278160e78d4
user:        Bryan O'Sullivan &lt;bos@serpentine.com&gt;
date:        Tue May 05 06:55:53 2009 +0000
summary:     Added an extra line of output

changeset:   5:5218ee8aecf3
user:        Bryan O'Sullivan &lt;bos@serpentine.com&gt;
date:        Tue May 05 06:55:55 2009 +0000
summary:     A new hello for a new day.

</pre></div><div class="sect2" lang="en"><div class="titlepage"><div><div><h3 class="title" id="id353830">Performing the merge</h3></div></div></div><p id="x_344"><a name="x_344"></a>What happens if we try to use the normal <span class="command"><strong>hg update</strong></span> command to update to the
	new tip?</p><pre id="id354203" class="screen"><code class="prompt">$</code> <strong class="userinput"><code>hg update</code></strong>
abort: crosses branches (use 'hg merge' or 'hg update -C')
</pre><p id="x_345"><a name="x_345"></a>Mercurial is telling us that the <span class="command"><strong>hg update</strong></span> command won't do a merge;
	it won't update the working directory when it thinks we might
	want to do a merge, unless we force it to do so.
	(Incidentally, forcing the update with <span class="command"><strong>hg update
	  -C</strong></span> would revert any uncommitted changes in the
	working directory.)</p><p id="x_723"><a name="x_723"></a>To start a merge between the two heads, we use the
	<span class="command"><strong>hg merge</strong></span> command.</p><pre id="id354163" class="screen"><code class="prompt">$</code> <strong class="userinput"><code>hg merge</code></strong>
merging hello.c
0 files updated, 1 files merged, 0 files removed, 0 files unresolved
(branch merge, don't forget to commit)
</pre><p id="x_347"><a name="x_347"></a>We resolve the contents of <code class="filename">hello.c</code>

This updates the working directory so that it
	contains changes from <span class="emphasis"><em>both</em></span> heads, which
	is reflected in both the output of <span class="command"><strong>hg
	  parents</strong></span> and the contents of
	<code class="filename">hello.c</code>.</p><pre id="id354360" class="screen"><code class="prompt">$</code> <strong class="userinput"><code>hg parents</code></strong>
changeset:   5:5218ee8aecf3
user:        Bryan O'Sullivan &lt;bos@serpentine.com&gt;
date:        Tue May 05 06:55:55 2009 +0000
summary:     A new hello for a new day.

changeset:   6:b6fed4f21233
tag:         tip
parent:      4:2278160e78d4
user:        Bryan O'Sullivan &lt;bos@serpentine.com&gt;
date:        Tue May 05 06:55:53 2009 +0000
summary:     Added an extra line of output

<code class="prompt">$</code> <strong class="userinput"><code>cat hello.c</code></strong>
/*
 * Placed in the public domain by Bryan O'Sullivan.  This program is
 * not covered by patents in the United States or other countries.
 */

#include &lt;stdio.h&gt;

int main(int argc, char **argv)
{
	printf("once more, hello.\n");
	printf("hello, world!\");
	printf("hello again!\n");
	return 0;
}
</pre></div><div class="sect2" lang="en"><div class="titlepage"><div><div><h3 class="title" id="id353997">Committing the results of the merge</h3></div></div></div><p id="x_348"><a name="x_348"></a>Whenever we've done a merge, <span class="command"><strong>hg
	  parents</strong></span> will display two parents until we <span class="command"><strong>hg commit</strong></span> the results of the
	  merge.</p><pre id="id354037" class="screen"><code class="prompt">$</code> <strong class="userinput"><code>hg commit -m 'Merged changes'</code></strong>
</pre><p id="x_349"><a name="x_349"></a>We now have a new tip revision; notice that it has
	<span class="emphasis"><em>both</em></span> of our former heads as its parents.
	These are the same revisions that were previously displayed by
	<span class="command"><strong>hg parents</strong></span>.</p><pre id="id354110" class="screen"><code class="prompt">$</code> <strong class="userinput"><code>hg tip</code></strong>
changeset:   7:ecb0e17b2a4e
tag:         tip
parent:      5:5218ee8aecf3
parent:      6:b6fed4f21233
user:        Bryan O'Sullivan &lt;bos@serpentine.com&gt;
date:        Tue May 05 06:55:56 2009 +0000
summary:     Merged changes

</pre><p id="x_34a"><a name="x_34a"></a>In <a class="xref" href="a-tour-of-mercurial-merging-work.html#fig:tour-merge:merge" title="Figure 3.3. Working directory and repository during merge, and following commit">Figure 3.3, “Working directory and repository during merge, and
	  following commit”</a>, you can see a
	representation of what happens to the working directory during
	the merge, and how this affects the repository when the commit
	happens.  During the merge, the working directory has two
	parent changesets, and these become the parents of the new
	changeset.</p><div class="figure"><a name="fig:tour-merge:merge"></a><p class="title"><b>Figure 3.3. Working directory and repository during merge, and
	  following commit</b></p><div class="figure-contents"><div class="mediaobject"><img src="figs/tour-merge-merge.png" alt="XXX add text"></div></div></div><br class="figure-break"><p id="x_69c"><a name="x_69c"></a>We sometimes talk about a merge having
	<span class="emphasis"><em>sides</em></span>: the left side is the first parent
	in the output of <span class="command"><strong>hg parents</strong></span>,
	and the right side is the second.  If the working directory
	was at e.g. revision 5 before we began a merge, that revision
	will become the left side of the merge.</p></div></div><div class="sect1" lang="en"><div class="titlepage"><div><div><h2 class="title" style="clear: both" id="id354262">Merging conflicting changes</h2></div></div></div><p id="x_34b"><a name="x_34b"></a>Most merges are simple affairs, but sometimes you'll find
      yourself merging changes where each side modifies the same portions
      of the same files.  Unless both modifications are identical,
      this results in a <span class="emphasis"><em>conflict</em></span>, where you have
      to decide how to reconcile the different changes into something
      coherent.</p><div class="figure"><a name="fig:tour-merge:conflict"></a><p class="title"><b>Figure 3.4. Conflicting changes to a document</b></p><div class="figure-contents"><div class="mediaobject"><img src="figs/tour-merge-conflict.png" alt="XXX add text"></div></div></div><br class="figure-break"><p id="x_34d"><a name="x_34d"></a><a class="xref" href="a-tour-of-mercurial-merging-work.html#fig:tour-merge:conflict" title="Figure 3.4. Conflicting changes to a document">Figure 3.4, “Conflicting changes to a document”</a> illustrates
      an instance of two conflicting changes to a document.  We
      started with a single version of the file; then we made some
      changes; while someone else made different changes to the same
      text.  Our task in resolving the conflicting changes is to
      decide what the file should look like.</p><p id="x_34e"><a name="x_34e"></a>Mercurial doesn't have a built-in facility for handling
      conflicts. Instead, it runs an external program, usually one
      that displays some kind of graphical conflict resolution
      interface.  By default, Mercurial tries to find one of several
      different merging tools that are likely to be installed on your
      system.  It first tries a few fully automatic merging tools; if
      these don't succeed (because the resolution process requires
      human guidance) or aren't present, it tries a few
      different graphical merging tools.</p><p id="x_34f"><a name="x_34f"></a>It's also possible to get Mercurial to run a
      specific program or script, by setting the
      <code class="envar">HGMERGE</code> environment variable to the name of your
      preferred program.</p><div class="sect2" lang="en"><div class="titlepage"><div><div><h3 class="title" id="id354379">Using a graphical merge tool</h3></div></div></div><p id="x_350"><a name="x_350"></a>My preferred graphical merge tool is
	<span class="command"><strong>kdiff3</strong></span>, which I'll use to describe the
	features that are common to graphical file merging tools.  You
	can see a screenshot of <span class="command"><strong>kdiff3</strong></span> in action in
	<a class="xref" href="a-tour-of-mercurial-merging-work.html#fig:tour-merge:kdiff3" title="Figure 3.5. Using kdiff3 to merge versions of a file">Figure 3.5, “Using kdiff3 to merge versions of a
	  file”</a>.  The kind of
	merge it is performing is called a <span class="emphasis"><em>three-way
	  merge</em></span>, because there are three different versions
	of the file of interest to us.  The tool thus splits the upper
	portion of the window into three panes:</p><div class="itemizedlist"><ul type="disc"><li><p id="x_351"><a name="x_351"></a>At the left is the <span class="emphasis"><em>base</em></span>
	    version of the file, i.e. the most recent version from
	    which the two versions we're trying to merge are
	    descended.</p></li><li><p id="x_352"><a name="x_352"></a>In the middle is “<span class="quote">our</span>” version of
	    the file, with the contents that we modified.</p></li><li><p id="x_353"><a name="x_353"></a>On the right is “<span class="quote">their</span>” version
	    of the file, the one that from the changeset that we're
	    trying to merge with.</p></li></ul></div><p id="x_354"><a name="x_354"></a>In the pane below these is the current
	<span class="emphasis"><em>result</em></span> of the merge. Our task is to
	replace all of the red text, which indicates unresolved
	conflicts, with some sensible merger of the
	“<span class="quote">ours</span>” and “<span class="quote">theirs</span>” versions of the
	file.</p><p id="x_355"><a name="x_355"></a>All four of these panes are <span class="emphasis"><em>locked
	  together</em></span>; if we scroll vertically or horizontally
	in any of them, the others are updated to display the
	corresponding sections of their respective files.</p><div class="figure"><a name="fig:tour-merge:kdiff3"></a><p class="title"><b>Figure 3.5. Using <span class="command">kdiff3</span> to merge versions of a
	  file</b></p><div class="figure-contents"><div class="mediaobject"><table border="0" summary="manufactured viewport for HTML img" cellspacing="0" cellpadding="0" width="100%"><tr><td><img src="figs/kdiff3.png" width="100%" alt="XXX add text"></td></tr></table></div></div></div><br class="figure-break"><p id="x_357"><a name="x_357"></a>For each conflicting portion of the file, we can choose to
	resolve the conflict using some combination of text from the
	base version, ours, or theirs.  We can also manually edit the
	merged file at any time, in case we need to make further
	modifications.</p><p id="x_358"><a name="x_358"></a>There are <span class="emphasis"><em>many</em></span> file merging tools
	available, too many to cover here.  They vary in which
	platforms they are available for, and in their particular
	strengths and weaknesses.  Most are tuned for merging files
	containing plain text, while a few are aimed at specialised
	file formats (generally XML).</p></div><div class="sect2" lang="en"><div class="titlepage"><div><div><h3 class="title" id="id354524">A worked example</h3></div></div></div><p id="x_359"><a name="x_359"></a>In this example, we will reproduce the file modification
	history of <a class="xref" href="a-tour-of-mercurial-merging-work.html#fig:tour-merge:conflict" title="Figure 3.4. Conflicting changes to a document">Figure 3.4, “Conflicting changes to a document”</a>
	above.  Let's begin by creating a repository with a base
	version of our document.</p><pre id="id354880" class="screen"><code class="prompt">$</code> <strong class="userinput"><code>cat &gt; letter.txt &lt;&lt;EOF</code></strong>
<code class="prompt">&gt;</code> <strong class="userinput"><code>Greetings!</code></strong>
<code class="prompt">&gt;</code> <strong class="userinput"><code>I am Mariam Abacha, the wife of former</code></strong>
<code class="prompt">&gt;</code> <strong class="userinput"><code>Nigerian dictator Sani Abacha.</code></strong>
<code class="prompt">&gt;</code> <strong class="userinput"><code>EOF</code></strong>
<code class="prompt">$</code> <strong class="userinput"><code>hg add letter.txt</code></strong>
<code class="prompt">$</code> <strong class="userinput"><code>hg commit -m '419 scam, first draft'</code></strong>
</pre><p id="x_35a"><a name="x_35a"></a>We'll clone the repository and make a change to the
	file.</p><pre id="id354860" class="screen"><code class="prompt">$</code> <strong class="userinput"><code>cd ..</code></strong>
<code class="prompt">$</code> <strong class="userinput"><code>hg clone scam scam-cousin</code></strong>
updating working directory
1 files updated, 0 files merged, 0 files removed, 0 files unresolved
<code class="prompt">$</code> <strong class="userinput"><code>cd scam-cousin</code></strong>
<code class="prompt">$</code> <strong class="userinput"><code>cat &gt; letter.txt &lt;&lt;EOF</code></strong>
<code class="prompt">&gt;</code> <strong class="userinput"><code>Greetings!</code></strong>
<code class="prompt">&gt;</code> <strong class="userinput"><code>I am Shehu Musa Abacha, cousin to the former</code></strong>
<code class="prompt">&gt;</code> <strong class="userinput"><code>Nigerian dictator Sani Abacha.</code></strong>
<code class="prompt">&gt;</code> <strong class="userinput"><code>EOF</code></strong>
<code class="prompt">$</code> <strong class="userinput"><code>hg commit -m '419 scam, with cousin'</code></strong>
</pre><p id="x_35b"><a name="x_35b"></a>And another clone, to simulate someone else making a
	change to the file. (This hints at the idea that it's not all
	that unusual to merge with yourself when you isolate tasks in
	separate repositories, and indeed to find and resolve
	conflicts while doing so.)</p><pre id="id354843" class="screen"><code class="prompt">$</code> <strong class="userinput"><code>cd ..</code></strong>
<code class="prompt">$</code> <strong class="userinput"><code>hg clone scam scam-son</code></strong>
updating working directory
1 files updated, 0 files merged, 0 files removed, 0 files unresolved
<code class="prompt">$</code> <strong class="userinput"><code>cd scam-son</code></strong>
<code class="prompt">$</code> <strong class="userinput"><code>cat &gt; letter.txt &lt;&lt;EOF</code></strong>
<code class="prompt">&gt;</code> <strong class="userinput"><code>Greetings!</code></strong>
<code class="prompt">&gt;</code> <strong class="userinput"><code>I am Alhaji Abba Abacha, son of the former</code></strong>
<code class="prompt">&gt;</code> <strong class="userinput"><code>Nigerian dictator Sani Abacha.</code></strong>
<code class="prompt">&gt;</code> <strong class="userinput"><code>EOF</code></strong>
<code class="prompt">$</code> <strong class="userinput"><code>hg commit -m '419 scam, with son'</code></strong>
</pre><p id="x_35c"><a name="x_35c"></a>Having created two
	different versions of the file, we'll set up an environment
	suitable for running our merge.</p><pre id="id355540" class="screen"><code class="prompt">$</code> <strong class="userinput"><code>cd ..</code></strong>
<code class="prompt">$</code> <strong class="userinput"><code>hg clone scam-cousin scam-merge</code></strong>
updating working directory
1 files updated, 0 files merged, 0 files removed, 0 files unresolved
<code class="prompt">$</code> <strong class="userinput"><code>cd scam-merge</code></strong>
<code class="prompt">$</code> <strong class="userinput"><code>hg pull -u ../scam-son</code></strong>
pulling from ../scam-son
searching for changes
adding changesets
adding manifests
adding file changes
added 1 changesets with 1 changes to 1 files (+1 heads)
not updating, since new heads added
(run 'hg heads' to see heads, 'hg merge' to merge)
</pre><p id="x_35d"><a name="x_35d"></a>In this example, I'll set
	<code class="envar">HGMERGE</code> to tell Mercurial to use the
	non-interactive <span class="command"><strong>merge</strong></span> command.  This is
	bundled with many Unix-like systems. (If you're following this
	example on your computer, don't bother setting
	<code class="envar">HGMERGE</code>.  You'll get dropped into a GUI file
	merge tool instead, which is much preferable.)</p><pre id="id355530" class="screen"><code class="prompt">$</code> <strong class="userinput"><code>export HGMERGE=merge</code></strong>
<code class="prompt">$</code> <strong class="userinput"><code>hg merge</code></strong>
merging letter.txt
merge: warning: conflicts during merge
merging letter.txt failed!
0 files updated, 0 files merged, 0 files removed, 1 files unresolved
use 'hg resolve' to retry unresolved file merges or 'hg up --clean' to abandon
<code class="prompt">$</code> <strong class="userinput"><code>cat letter.txt</code></strong>
Greetings!
&lt;&lt;&lt;&lt;&lt;&lt;&lt; /tmp/tour-merge-conflictgW7-1Z/scam-merge/letter.txt
I am Shehu Musa Abacha, cousin to the former
=======
I am Alhaji Abba Abacha, son of the former
&gt;&gt;&gt;&gt;&gt;&gt;&gt; /tmp/letter.txt~other.c6Rq0s
Nigerian dictator Sani Abacha.
</pre><p id="x_35f"><a name="x_35f"></a>Because <span class="command"><strong>merge</strong></span> can't resolve the
	conflicting changes, it leaves <span class="emphasis"><em>merge
	  markers</em></span> inside the file that has conflicts,
	indicating which lines have conflicts, and whether they came
	from our version of the file or theirs.</p><p id="x_360"><a name="x_360"></a>Mercurial can tell from the way <span class="command"><strong>merge</strong></span>
	exits that it wasn't able to merge successfully, so it tells
	us what commands we'll need to run if we want to redo the
	merging operation.  This could be useful if, for example, we
	were running a graphical merge tool and quit because we were
	confused or realised we had made a mistake.</p><p id="x_361"><a name="x_361"></a>If automatic or manual merges fail, there's nothing to
	prevent us from “<span class="quote">fixing up</span>” the affected files
	ourselves, and committing the results of our merge:</p><pre id="id355496" class="screen"><code class="prompt">$</code> <strong class="userinput"><code>cat &gt; letter.txt &lt;&lt;EOF</code></strong>
<code class="prompt">&gt;</code> <strong class="userinput"><code>Greetings!</code></strong>
<code class="prompt">&gt;</code> <strong class="userinput"><code>I am Bryan O'Sullivan, no relation of the former</code></strong>
<code class="prompt">&gt;</code> <strong class="userinput"><code>Nigerian dictator Sani Abacha.</code></strong>
<code class="prompt">&gt;</code> <strong class="userinput"><code>EOF</code></strong>
<code class="prompt">$</code> <strong class="userinput"><code>hg resolve -m letter.txt</code></strong>
<code class="prompt">$</code> <strong class="userinput"><code>hg commit -m 'Send me your money'</code></strong>
<code class="prompt">$</code> <strong class="userinput"><code>hg tip</code></strong>
changeset:   3:6f17ad930bf5
tag:         tip
parent:      1:cef8fbca9a6f
parent:      2:dc8f64391590
user:        Bryan O'Sullivan &lt;bos@serpentine.com&gt;
date:        Tue May 05 06:55:57 2009 +0000
summary:     Send me your money

</pre><div class="note"><table border="0" summary="Note: Where is the hg resolve command?"><tr><td rowspan="2" align="center" valign="top" width="25"><img alt="[Note]" src="/support/figs/note.png"></td><th align="left">Where is the hg resolve command?</th></tr><tr><td align="left" valign="top"><p id="x_724"><a name="x_724"></a>The <span class="command"><strong>hg resolve</strong></span> command was introduced
	  in Mercurial 1.1, which was released in December 2008. If
	  you are using an older version of Mercurial (run <span class="command"><strong>hg
	    version</strong></span> to see), this command will not be
	  present.  If your version of Mercurial is older than 1.1,
	  you should strongly consider upgrading to a newer version
	  before trying to tackle complicated merges.</p></td></tr></table></div></div></div><div class="sect1" lang="en"><div class="titlepage"><div><div><h2 class="title" style="clear: both" id="sec:tour-merge:fetch">Simplifying the pull-merge-commit sequence</h2></div></div></div><p id="x_362"><a name="x_362"></a>The process of merging changes as outlined above is
      straightforward, but requires running three commands in
      sequence.</p><pre id="id355698" class="programlisting">hg pull -u
hg merge
hg commit -m 'Merged remote changes'</pre><p id="x_363"><a name="x_363"></a>In the case of the final commit, you also need to enter a
      commit message, which is almost always going to be a piece of
      uninteresting “<span class="quote">boilerplate</span>” text.</p><p id="x_364"><a name="x_364"></a>It would be nice to reduce the number of steps needed, if
      this were possible.  Indeed, Mercurial is distributed with an
      extension called <code class="literal">fetch</code> that
      does just this.</p><p id="x_365"><a name="x_365"></a>Mercurial provides a flexible extension mechanism that lets
      people extend its functionality, while keeping the core of
      Mercurial small and easy to deal with.  Some extensions add new
      commands that you can use from the command line, while others
      work “<span class="quote">behind the scenes,</span>” for example adding
      capabilities to Mercurial's built-in server mode.</p><p id="x_366"><a name="x_366"></a>The <code class="literal">fetch</code>
      extension adds a new command called, not surprisingly, <span class="command"><strong>hg fetch</strong></span>.  This extension acts as a
      combination of <span class="command"><strong>hg pull -u</strong></span>,
      <span class="command"><strong>hg merge</strong></span> and <span class="command"><strong>hg commit</strong></span>.  It begins by pulling
      changes from another repository into the current repository.  If
      it finds that the changes added a new head to the repository, it
      updates to the new head, begins a merge, then (if the merge
      succeeded) commits the result of the merge with an
      automatically-generated commit message.  If no new heads were
      added, it updates the working directory to the new tip
      changeset.</p><p id="x_367"><a name="x_367"></a>Enabling the <code class="literal">fetch</code> extension is easy.  Edit the
      <code class="filename">.hgrc</code> file in your home
      directory, and either go to the <code class="literal">extensions</code> section or create an
      <code class="literal">extensions</code> section. Then
      add a line that simply reads
      “<span class="quote"><code class="literal">fetch=</code></span>”.</p><pre id="id355838" class="programlisting">[extensions]
fetch =</pre><p id="x_368"><a name="x_368"></a>(Normally, the right-hand side of the
      “<span class="quote"><code class="literal">=</code></span>” would indicate where to find
      the extension, but since the <code class="literal">fetch</code> extension is in the standard
      distribution, Mercurial knows where to search for it.)</p></div><div class="sect1" lang="en"><div class="titlepage"><div><div><h2 class="title" style="clear: both" id="id355904">Renaming, copying, and merging</h2></div></div></div><p id="x_729"><a name="x_729"></a>During the life of a project, we will often want to change
      the layout of its files and directories. This can be as simple
      as renaming a single file, or as complex as restructuring the
      entire hierarchy of files within the project.</p><p id="x_72a"><a name="x_72a"></a>Mercurial supports these kinds of complex changes fluently,
      provided we tell it what we're doing.  If we want to rename a
      file, we should use the <span class="command"><strong>hg rename</strong></span><sup>[<a name="id355926" href="#ftn.id355926" class="footnote">2</a>]</sup> command to rename it, so that Mercurial can do the
      right thing later when we merge.</p><p id="x_72c"><a name="x_72c"></a>We will cover the use of these commands in more detail in
      <a class="xref" href="mercurial-in-daily-use.html#chap:daily.copy" title="Copying files">the section called “Copying files”</a>.</p></div><div class="footnotes"><br><hr width="100" align="left"><div class="footnote"><p><sup>[<a name="ftn.id355926" href="#id355926" class="para">2</a>] </sup>If you're a Unix user, you'll be glad to know that the
	  <span class="command"><strong>hg rename</strong></span> command can be abbreviated as
	  <span class="command"><strong>hg mv</strong></span>.</p></div></div></div><div class="hgfooter"><p><img src="/support/figs/rss.png"> Want to stay up to date? Subscribe to the comment feed for <a id="chapterfeed" class="feed" href="/feeds/comments/">this chapter</a>, or the <a class="feed" href="/feeds/comments/">entire book</a>.</p><p>Copyright 2006, 2007, 2008, 2009 Bryan O'Sullivan.
      Icons by <a href="mailto:mattahan@gmail.com">Paul Davey</a> aka <a href="http://mattahan.deviantart.com/">Mattahan</a>.</p></div><div class="navfooter"><table width="100%" summary="Navigation footer"><tr><td width="40%" align="left"><a accesskey="p" href="a-tour-of-mercurial-the-basics.html">Prev</a> </td><td width="20%" align="center"> </td><td width="40%" align="right"> <a accesskey="n" href="behind-the-scenes.html">Next</a></td></tr><tr><td width="40%" align="left" valign="top">Chapter 2. A tour of Mercurial: the basics </td><td width="20%" align="center"><a accesskey="h" href="index.html">Home</a></td><td width="40%" align="right" valign="top"> Chapter 4. Behind the scenes</td></tr></table></div><script type="text/javascript">
    var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
    document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
    </script><script type="text/javascript">
    try {
    var pageTracker = _gat._getTracker("UA-1805907-5");
    pageTracker._trackPageview();
    } catch(err) {}</script></body></html>
